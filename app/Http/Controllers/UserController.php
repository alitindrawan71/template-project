<?php

namespace App\Http\Controllers;

use Validator;

use Illuminate\Http\Request;
Use Illuminate\Support\Facades\DB;
Use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Yajra\Datatables\Datatables;

use App\User;

use Carbon\Carbon;

class UserController extends Controller
{
    public $var;
    public function __construct(){
        $this->var['title'] = "User";
    }

    public function index(){
        $var = $this->var;
        return view('admin.module.user.index', compact('var'));
    }

    public function create(){
        $var = $this->var;
        return view('admin.module.user.create', compact('var'));
    }

    public function store(Request $request){        
        $validator = Validator::make($request->input(), array(
            "username"          => 'required',            
            "email"             => 'required',
            "password"          => 'required|min:6|same:repassword',
            "repassword"        => 'required',
        ));
        
        if ($validator->fails()) {
            return response()->json([
                'message'   => $validator->errors(),
                'status'    => false
            ], 400);
        }
            
        try {
            DB::beginTransaction();
            $data_user = User::create([
                'username'      => $request->username,                
                'email'         => $request->email,     
                'password'      => bcrypt($request->password),
            ]);            

            DB::commit();
            return response()->json(['status' => true]);
        } catch (\Throwable $th) {
            DB::rollback();
            return response()->json(['status' => false, 'message' => $th->getMessage()], 400);
        }
    }

    public function show($id){
        $var = $this->var;
        $data = User::find($id);
        return view('admin.module.user.show',compact('var','data'));
    }

    public function edit($id){
        $var = $this->var;
        $data = User::find($id);
        return view('admin.module.user.edit', compact('var','data'));
    }
    

    public function update($id,Request $request){

        $validator = Validator::make($request->input(), array(
            "username"          => 'required',
            "email"             => 'required',
            "password"          => 'nullable|min:6|same:repassword',            
        ));
        
        if ($validator->fails()) {
            return response()->json([
                'message'   => $validator->errors(),
                'status'    => false
            ], 400);
        }
            
        try {
            DB::beginTransaction();

            $data_user = User::find($id);
            if($request->password){
                $data_user->update([
                    'username'      => $request->username,                
                    'email'         => $request->email,
                    'password'      => bcrypt($request->password),
                ]);
            }
            else{
                $data_user->update([
                    'username'      => $request->username,                
                    'email'         => $request->email,
                ]);            
            }            

            DB::commit();
            return response()->json(['status' => true]);
        } catch (\Throwable $th) {
            DB::rollback();
            return response()->json(['status' => false, 'message' => $th->getMessage()], 400);
        }
    }

    public function delete($id){
        try {
            DB::beginTransaction();

            $data = User::find($id);
            $data->delete();

            DB::commit();
            return response()->json(['status'=>true]);
        } catch (\Throwable $th) {
            DB::rollback();
            return response()->json(['status'=>false,'message'=>$th->getMessage() ],400);
        }        
    }

    public function getData(){
        $query = User::latest();
        return Datatables::of($query)
        ->addIndexColumn()
        ->addColumn('action', function($data){
            $link = url('user/delete').'/'.$data->id;
            return "
                <a href=".url('user/'.$data->id)."><button class='btn btn-sm btn-secondary'><i class='fa fa-eye'></i><span class='d-none d-sm-inline'> Lihat</span></button></a>
                <a href=".url('user/'.$data->id.'/edit')."><button class='btn btn-sm btn-success'><i class='fa fa-edit'></i><span class='d-none d-sm-inline'> Edit</span></button></a>
                <button onclick='del(`$link`,`".$data->username."`)' class='btn btn-sm btn-danger'><i class='fa fa-trash'></i><span class='d-none d-sm-inline'> Hapus</span></button>
            ";
        })
        ->rawColumns([
            'action',
        ])
        ->make(true);
    }
}
