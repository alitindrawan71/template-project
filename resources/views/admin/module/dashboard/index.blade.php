@extends('admin.layouts.app', ['active' => 'Dashboard'])
@section('content')

<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
    <div class="row mb-2">
        <div class="col-sm-6">
        <h1>Dashboard</h1>
        </div>
        <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item active">Home</li>
        </ol>
        </div>
    </div>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
    <div class="container-fluid">        
        <div class="row">
            <div class="col-lg-3 col-6">            
                <div class="small-box bg-info">
                    <div class="inner">
                        <h3>{{ App\User::count() }}</h3>
                        <p>User</p>
                    </div>
                    <div class="icon">
                        <i class="nav-icon fas fa-user"></i>
                    </div>
                    <a href="{{ route('user.index') }}" class="small-box-footer">Lihat <i class="fas fa-arrow-circle-right"></i></a>
                </div>
            </div>
            
        </div> 
    </div>
</section>
<!-- /.content -->
@endsection
@section('javascripts')

@endsection