@extends('admin.layouts.app', ['active' => $var['title'] ])

@section('content')

<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
    <div class="row mb-2">
        <div class="col-sm-6">
        <h1>{{ $var['title'] }}</h1>
        </div>
        <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
            <li class="breadcrumb-item active">{{ $var['title'] }}</li>
        </ol>
        </div>
    </div>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
    <div class="float-right mb-3">
        <a href="{{ route('user.create') }}"><button class="btn btn-primary btn-sm"><i class="fa fa-plus"></i> <span class="d-none d-sm-inline">Tambah {{ $var['title'] }}</span></button></a>
    </div>
    <div class="clearfix"></div>
    <!-- Default box -->
    <div class="card">
        <div class="card-header">
            <h3 class="card-title"><i class="fa fa-user"></i> List {{ $var['title'] }}</h3>
            <div class="card-tools">
                <button onclick="$('#tableData').DataTable().ajax.reload();" type="button" class="btn btn-tool" data-toggle="tooltip" title="Refresh">
                    <i class="fas fa-sync"></i></button>                
                <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i class="fas fa-minus"></i></button>
            </div>
        </div>        
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-striped  m-0" id="tableData">
                    <thead>
                        <tr>
                            <th>No.</th>                            
                            <th>Username</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>    
    </div>
</section>
<!-- /.content -->
@endsection

@section('javascripts')
    <script>
        $(function() {
            var oTable = $('#tableData').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: '{{ route("user_getData") }}'
                },
                columns: [              
                {data: 'DT_RowIndex', name: 'DT_RowIndex',searchable:false, orderable: false, width:"10%"},
                {data: 'username', name: 'username'},
                {data: 'action', name: 'action', orderable:false, searchable:false, width:"25%"},
                ],
            });
            
        });

        function del(route,nama){
            Swal.fire({
                title: 'Peringatan!',
                text: "Yakin menghapus data "+nama+" ?",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#d33',
                cancelButtonColor: '#3085d6',
                confirmButtonText: 'Hapus',
                cancelButtonText: 'Batalkan',
                showLoaderOnConfirm: true,
                preConfirm: (del) => {
                    return fetch(route,{
                    method: 'get',
                    credentials: "same-origin",
                    })
                    .then(response => {
                        if (!response.ok) {
                        throw new Error(response.statusText)
                        }
                        return response.json()
                    })
                    .catch(error => {
                        Swal.showValidationMessage(
                        `Request failed: ${error}`
                        )
                    })
                },
                allowOutsideClick: () => !Swal.isLoading()
                }).then((result) => {                    
                if (result.value) {                    
                    Swal.fire(
                        'Terhapus!',
                        'Data berhasil dihapus',
                        'success'
                    )
                    $('#tableData').DataTable().ajax.reload();
                }
                })
        }
        
    </script>
@endsection